package idnlab.iacopodeenosee.test_zip;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class FileActivity extends AppCompatActivity {

    TextView tvw_filename,tvw_filecontent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file);

        tvw_filename = findViewById(R.id.txw_filename);
        tvw_filecontent=findViewById(R.id.txw_filecontent);

        Intent intent = getIntent();

        String pkg = getPackageName();
        String fileName = intent.getStringExtra(pkg + ".strName");
        String fileContent = intent.getStringExtra(pkg + ".strContent");

        if (fileName.equals("")) {
            Toast.makeText(getApplicationContext(), "STRING XML EMPTY", Toast.LENGTH_LONG);

            tvw_filename.setText("file NAME: \n\n" + "ERROR");
            tvw_filename.setTextColor(Color.BLACK);

        } else {
            tvw_filename.setText("file: " + fileName);
            tvw_filename.setTextColor(Color.RED);

            tvw_filecontent.setText(fileContent);
            tvw_filecontent.setTextColor(Color.BLUE);
        }
    }

}
