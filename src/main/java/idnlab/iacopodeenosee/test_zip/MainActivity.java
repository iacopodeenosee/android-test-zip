package idnlab.iacopodeenosee.test_zip;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class MainActivity extends AppCompatActivity {

    Button btnLoadZip, btnGetFile;
    TextView txwListZip;
    Spinner spnListZip;
    String strFileName, strFileContent, strListZip;


    ArrayList<String> arrayContent = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        strFileName = strFileContent = strListZip = "";

        txwListZip = findViewById(R.id.txw_listzip);
        spnListZip = findViewById(R.id.spn_listzip);

        btnLoadZip = findViewById(R.id.btn_loadzip);
        btnLoadZip.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                strListZip = "";
                unzip("test_profile.zip");

                txwListZip.setText(strListZip);
                txwListZip.setTextColor(Color.BLACK);

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(view.getContext(),
                        android.R.layout.simple_spinner_item, arrayContent);
                spnListZip.setAdapter(adapter);
            }
        });


        btnGetFile = findViewById(R.id.btn_getFILE);
        btnGetFile.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                if (!strFileName.equals("")) {
                    //load a selected file
                    strFileContent = loadZipFile(strFileName);

                    String pkg = getPackageName();
                    Intent i = new Intent(getApplicationContext(), FileActivity.class);

                    i.putExtra(pkg + ".strName", strFileName);
                    i.putExtra(pkg + ".strContent", strFileContent);

                    view.getContext().startActivity(i);
                }
            }
        });

        spnListZip.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Object item = adapterView.getItemAtPosition(i);
                strFileName = item.toString();

                Log.v("test_ZIP", "file selected: " + strFileName);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    public void unzip(String zipFile) {
        AssetManager assetManager = getAssets();
        InputStream inputStream = null;
        try {
            inputStream = assetManager.open(zipFile);
        } catch (IOException e) {
            Log.e("TEST_XML", e.getMessage());
        }

        ZipInputStream zipIs = null;
        try {
            // Create ZipInputStream object to read a file from path.
            zipIs = new ZipInputStream(inputStream);

            ZipEntry entry = null;
            // Read ever Entry (From top to bottom until the end)
            while ((entry = zipIs.getNextEntry()) != null) {
                String entryName = entry.getName();

                if (entry.isDirectory()) {
                    // Make directories.
                    Log.v("test_ZIP", "directory found: " + entryName);
                    strListZip += "D-> " + entryName + "\n";
                } else {
                    Log.v("test_ZIP", "file found: " + entryName);

                    strListZip += "F-> " + entryName + "\n";

                    this.arrayContent.add(entryName);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("test_ZIP", e.getMessage());
        } finally {
            try {
                zipIs.close();
            } catch (Exception e) {
                Log.e("test_ZIP", e.getMessage());
            }
        }
    }

    private String loadZipFile(String file) {
        String tmpFile = "";

        AssetManager assetManager = getAssets();
        InputStream inputStream = null;
        try {
            inputStream = assetManager.open("test_profile.zip");
        } catch (IOException e) {
            Log.e("TEST_XML", e.getMessage());
        }

        ZipInputStream zipIs = null;
        try {
            // Create ZipInputStream object to read a file from path.
            zipIs = new ZipInputStream(inputStream);

            ZipEntry entry = null;
            // Read ever Entry (From top to bottom until the end)
            while ((entry = zipIs.getNextEntry()) != null) {
                String entryName = entry.getName();

                if (entry.isDirectory()) {
                    // Make directories.
                    Log.v("test_ZIP", "directory found: " + entryName);
                } else {
                    Log.v("test_ZIP", "file found: " + entryName);

                    if (entryName.equals(file)) {
                        tmpFile = readTextFile(zipIs);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("test_ZIP", e.getMessage());
        } finally {
            try {
                zipIs.close();
            } catch (Exception e) {
                Log.e("test_ZIP", e.getMessage());
            }
        }

        return tmpFile;
    }

    private String readTextFile(InputStream inputStream) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        byte buffer[] = new byte[1024];
        int size;

        try {
            while ((size = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, size);
            }
            outputStream.close();
            //inputStream.close();
        } catch (IOException e) {
            Log.e("test_ZIP", e.getMessage());
            return e.getMessage();
        }
        return outputStream.toString();
    }
}
